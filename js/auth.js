let forms = document.querySelectorAll('form')

// Main api route
let api = "https://finance-app-wepro.herokuapp.com"

const loader = document.querySelector("#loading");


function displayLoading() {
    loader.classList.add("display");
    // to stop loading after some time
    setTimeout(() => {
        loader.classList.remove("display");
    }, 5000);
}
function hideLoading() {
    loader.classList.remove("display");
}


for (let item of forms) {


    item.onsubmit = () => {
        event.preventDefault()
        displayLoading()
        let obj = {}
        let fm = new FormData(event.target)

        fm.forEach((a, b) => {
            obj[b] = a
        })
        let dataApi = item.getAttribute("data-api")
        axios.post(api + item.getAttribute("data-api"), obj)
        // Обработка ответа/ошибки
        .then(res => {
            if (res.status == 200 || res.status == 201) {
                hideLoading()
                console.log('done');
                // Сохраняем пользователя локально
                localStorage.user = JSON.stringify(res.data)
                // Редирект
                window.location.href = "./page_1.html"
                

            } else {
                // Бекенд говно
                hideLoading()
                throw new Error("Fucking server")
            }
        })
        // Фронт говно
        .catch(err => {
            throw new Error("Fucking client")
        })


        // Отправка данных на сервер по документации Postman

    }
}