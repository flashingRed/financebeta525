// Подключается во все страницы, в которых нужен user
let user = {},
    names = document.querySelectorAll('.name'),
    emails = document.querySelectorAll('.email'),
    quit = document.querySelector('.log_out')

if (localStorage.user) {
    // Достаем юзера из локалки и превращаем
    // в нормальный объект
    user = JSON.parse(localStorage.user)
    emails.forEach(email => {
        email.innerHTML = user.email
    })
    names.forEach(name => {
        name.innerHTML = user.name + ' ' + user.secondName
    })
    quit.onclick = () => {
        localStorage.clear(user)
        window.location.href = "./sign_in.html"
    }

} else {
    window.location.href = "./sign_up.html"
}
 